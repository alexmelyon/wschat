﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatController : MonoBehaviour
{
	
	private List<ChatHistory> historyListeners = new List<ChatHistory>();
	private WebSocket websocket;

	IEnumerator Start()
	{
		websocket = new WebSocket(new System.Uri("ws://127.0.0.1:8080/"));
		yield return StartCoroutine(websocket.Connect());
		Debug.Log("Connected");
		while(true) {
			string reply = websocket.RecvString();
			if(reply != null) {
				OnMessage(reply);
			}
			if(websocket.error != null) {
				Debug.LogError("Error: " + websocket.error);
				break;
			}
			yield return 0;
		}
		websocket.Close();
	}

	void OnDestroy() {
		if(websocket != null) {
			websocket.Close();
		}
	}

	public void AddHistoryListener(ChatHistory listener)
	{
		historyListeners.Add(listener);
	}

	public void RemoveHistoryListener(ChatHistory listener)
	{
		historyListeners.Remove(listener);
	}

	public void Send(string message)
	{
		if(websocket == null) {
			Debug.LogError("Not connected");
			return;
		}
		websocket.SendString(message);
	}

	public void OnMessage(string message) {
		historyListeners.ForEach(it => it.AddMessage(message));
	}
}

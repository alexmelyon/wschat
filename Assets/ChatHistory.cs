﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatHistory : MonoBehaviour
{
	[SerializeField]
	private Text text;

	[SerializeField]
	ChatController chatController;

	void Awake()
	{
		if(text == null) {
			text = GetComponent<Text>();
		}
	}

	void OnEnable()
	{
		chatController.AddHistoryListener(this);
	}

	void OnDisable()
	{
		chatController.RemoveHistoryListener(this);
	}

	public void AddMessage(string message)
	{
		GetComponent<Text>().text += "\n" + message;
	}
}

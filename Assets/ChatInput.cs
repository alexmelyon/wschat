﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChatInput : MonoBehaviour
{
	[SerializeField]
	InputField inputField;

	[SerializeField]
	ChatController chatController;

	void Awake()
	{
		if(!inputField) {
			inputField = GetComponent<InputField>();
		}
		inputField.onEndEdit.AddListener(OnEndEdit);
	}

	void OnEndEdit(string text)
	{
		chatController.Send(text);
		inputField.text = "";

		if(EventSystem.current.currentSelectedGameObject != inputField.gameObject) {
			EventSystem.current.SetSelectedGameObject(inputField.gameObject, null);
		}
		inputField.OnPointerClick(new PointerEventData(EventSystem.current));
	}
}

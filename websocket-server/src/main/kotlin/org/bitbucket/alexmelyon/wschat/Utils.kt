package org.bitbucket.alexmelyon.wschat

import java.io.IOException
import java.net.DatagramSocket
import java.net.ServerSocket


/**
 * Checks to see if a specific port is available.
 *
 * @param port the port to check for availability
 */
fun portAvailable(port: Int): Boolean {
    var server: ServerSocket? = null
    var datagram: DatagramSocket? = null
    try {
        server = ServerSocket(port)
        server.reuseAddress = true
        datagram = DatagramSocket(port)
        datagram.reuseAddress = true
        return true
    } catch (e: IOException) {
    } finally {
        if (datagram != null) {
            datagram.close()
        }

        if (server != null) {
            try {
                server.close()
            } catch (e: IOException) {
                /* should not be thrown */
            }

        }
    }

    return false
}
package org.bitbucket.alexmelyon.wschat

import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.net.InetSocketAddress

class Main(address: InetSocketAddress) : WebSocketServer(address) {


    companion object {

        private val DEFAULT_PORT = 8080

        @JvmStatic
        fun main(args: Array<String>) {
            println("Using: java -jar server.jar <port>")
            val a = listOf("one")
            val port = a.firstOrNull()?.toIntOrNull() ?: DEFAULT_PORT
            if(!portAvailable(port)) {
                println("Port $port already in use")
                System.exit(1)
            }
            val server = Main(InetSocketAddress(port))
            server.start()

            val sysin = BufferedReader(InputStreamReader(System.`in`))
            while (true) {
                val input = sysin.readLine()
                server.broadcast(input)
                if (input == "stop yes") {
                    server.stop(1000)
                    break
                }
            }
        }
    }

    override fun onOpen(conn: WebSocket, handshake: ClientHandshake) {
        println("${conn.remoteSocketAddress} Opened")
    }

    override fun onClose(conn: WebSocket, code: Int, reason: String, remote: Boolean) {
        println("${conn.remoteSocketAddress} Closed")
    }

    override fun onMessage(conn: WebSocket, message: String) {
        broadcast(message)
    }

    override fun onStart() {
        println("Started on $address")
    }

    override fun onError(conn: WebSocket?, ex: Exception) {
        System.err.println("${conn?.remoteSocketAddress} ${ex.message}\n${ex.stackTrace}")
    }

}
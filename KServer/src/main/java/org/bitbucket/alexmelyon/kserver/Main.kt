package org.bitbucket.alexmelyon.kserver

import java.net.Socket

fun main(args: Array<String>) {
    val kServer = KServer()
    kServer.listeners += object : KServerListener {
        override fun onStart() {
            println("STARTED")
        }

        override fun onOpen(socket: Socket?) {
            println("OPEN")
        }

        override fun onMessage(socket: Socket, message: String) {
            println("MESSAGE $message")
            kServer.broadcast(message)
        }

        override fun onError(ex: Exception) {
            println("ERROR ${ex.message}")
        }

        override fun onClose(socket: Socket) {
            println("CLOSE")
        }

    }
    val thread = kServer.start()
    thread.join()
}
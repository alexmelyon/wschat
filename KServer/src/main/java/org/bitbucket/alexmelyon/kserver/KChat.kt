package org.bitbucket.alexmelyon.kserver

import java.net.Socket

class KChat : KServerListener {

    companion object {
        val SECRET_STRING = "secret_string"
        @JvmStatic
        fun main(args: Array<String>) {
            val server = KChat().kServer.start()
            server.join()
        }
    }

    val kServer = KServer()
    val clients = mutableMapOf<Socket, String?>()
    val history = mutableListOf<String>()
    val commands = mutableMapOf<String, Socket>()

    init {
        kServer.listeners.add(this)
    }

    override fun onStart() {
        println("Started")
    }

    override fun onOpen(socket: Socket) {
        clients.put(socket, null)
        kServer.broadcast("Please enter your name:", listOf(socket))
    }

    override fun onMessage(socket: Socket, message: String) {
        println(message)
        val nick = clients[socket]
        if (nick == null) {
            if (message.startsWith(SECRET_STRING)) {
                val payload = message.substringAfter(":")
                if(payload.startsWith("commands")) {
                    val commandsList = payload.substringAfter(":").split(";")
                    commandsList.forEach { commands.put(it, socket) }
                } else if(payload.startsWith("answer")) {
                    val text = payload.substringAfter(":")
                    kServer.broadcast(text)
                }
            } else {
                clients[socket] = message
                history.forEach { kServer.broadcast(it, listOf(socket)) }
                kServer.broadcast("Your nickname is $message", listOf(socket))
                kServer.broadcast("Type '/help' for commands list", listOf(socket))
            }
        } else {
            if (message.startsWith("/")) {
                val command = message.substringBefore(" ")
                if(command == "/help") {
                    commands.keys.sorted().forEach { kServer.broadcast(it, listOf(socket)) }
                } else {
                    val helper = commands[command]
                    if (helper == null) {
                        kServer.broadcast("No such command '$command'")
                    } else {
                        kServer.broadcast("$nick:$command", listOf(helper))
                    }
                }
            } else {
                val line = "$nick: $message"
                updateHistory(line)
                kServer.broadcast(line)
            }
        }
    }

    fun updateHistory(message: String) {
        history.add(message)
        val count = history.size
        if (count > 100) {
            history.drop(count - 100)
        }
    }

    override fun onError(ex: Exception) {
        ex.printStackTrace()
    }

    override fun onClose(socket: Socket) {
        if(commands.values.contains(socket)) {
            val drop = commands.filterValues { it == socket }
            drop.forEach {
                println("Removed command ${it.key}")
                commands.remove(it.key)
            }
        } else {
            val nick = clients[socket]!!
            println("$nick leave")
            kServer.broadcast("$nick leave")
            clients.remove(socket)
        }
    }
}
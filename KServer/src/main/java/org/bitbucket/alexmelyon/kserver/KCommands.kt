package org.bitbucket.alexmelyon.kserver

import java.io.File
import java.nio.charset.StandardCharsets

class KCommands : KClientListener {

    companion object {

        val SECRET_STRING = "secret_string"

        @JvmStatic
        fun main(args: Array<String>) {
            KCommands()
        }
    }

    val kClient = KClient()
    val commands = listOf("/99")
    // 99 bottles
    val index = mutableMapOf<String, Int>()
    val bottles: List<List<String>>

    init {
        kClient.listeners += this
        kClient.connect()
        //
        val bottlesText = File("src/main/resources/bottles.txt").inputStream().bufferedReader(StandardCharsets.UTF_8).readLines().joinToString("\n")
        bottles = bottlesText.split("\n\n").map { it.split("\n") }
    }

    override fun onOpen() {
        val commandsJoined = commands.joinToString(";")
        kClient.send("$SECRET_STRING:commands:$commandsJoined")
    }

    override fun onMessage(message: String) {
        val nick = message.substringBefore(":")
        val command = message.substringAfter(":")
        when (command) {
            "/99" -> bottles(nick)
        }
    }

    fun bottles(nick: String) {
        val i = index[nick] ?: 0
        val lines = bottles[i]
        lines.forEach {
            kClient.send("$SECRET_STRING:answer:$it")
        }
        index[nick] = (i + 1) % bottles.size
    }
}
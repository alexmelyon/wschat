package org.bitbucket.alexmelyon.kserver

import java.net.Socket
import java.nio.charset.StandardCharsets
import java.util.*

class KClient(val host: String = "localhost", val port: Int = 8080) {
    val listeners = mutableListOf<KClientListener>()
    private lateinit var socket: Socket
    fun connect() {
        socket = Socket(host, port)
        listeners.forEach { it.onOpen() }
        val readerThread = Thread {
            while (true) {
                val reader = socket.getInputStream().bufferedReader(StandardCharsets.UTF_8)
                val input = reader.readLine().trim()
                if (input.isNotBlank()) {
                    listeners.forEach { it.onMessage(input) }
                }
            }
        }
        readerThread.start()
    }

    fun send(message: String) {
        if (message == "exit") {
            socket.getOutputStream().close()
        }
        val writer = socket.getOutputStream().bufferedWriter(StandardCharsets.UTF_8)
        writer.write(message)
        writer.newLine()
        writer.flush()
    }
}

fun main(args: Array<String>) {
    val host = args.firstOrNull() ?: "localhost"
    val port = args.getOrNull(1)?.toInt() ?: 8080

    val client = KClient(host, port)
    client.listeners += object : KClientListener {
        override fun onOpen() {
            println("Opened")
        }

        override fun onMessage(message: String) {
            println("< $message")
        }

    }
    client.connect()
    val scanner = Scanner(System.`in`)
    while (true) {
        print("> ")
        val line = scanner.next()
        client.send(line)
    }
}
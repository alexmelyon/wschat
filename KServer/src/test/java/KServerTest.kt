import org.bitbucket.alexmelyon.kserver.KClient
import org.bitbucket.alexmelyon.kserver.KServer
import org.bitbucket.alexmelyon.kserver.KServerListener
import org.mockito.Matchers.any
import org.mockito.Matchers.eq
import org.mockito.Mockito
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import java.net.Socket

class KServerTest {

    val listener = Mockito.mock(TestKServerListener::class.java)

    @BeforeMethod
    fun setUp() {
        val server = KServer()
        server.listeners += listener
        server.start()
    }

    @Test
    fun testStart() {
        Mockito.verify(listener).onStart()
    }

    @Test
    fun testConnect() {
        val client = KClient()
        client.connect()
        Mockito.verify(listener).onOpen(any())
    }

    @Test
    fun testMessage() {
        val client = KClient()
        client.connect()
        client.send("message")
        Mockito.verify(listener).onMessage(any(Socket::class.java), eq("message"))
    }

    open class TestKServerListener : KServerListener {
        override fun onStart() {}

        override fun onOpen(socket: Socket) {}

        override fun onMessage(socket: Socket, message: String) {}

        override fun onError(ex: Exception) {}

        override fun onClose(socket: Socket) {}
    }
}